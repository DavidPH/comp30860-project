mvn package
docker-compose build
docker-compose up

If the docker commands don't work, you can swap the comments around in the application.properties file and use springboot instead. mvn spring-boot:run