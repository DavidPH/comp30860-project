package WorkingTitle.model;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class UserSession{
    private User user;
    private boolean isLoginFailed;
    private boolean nameIsUnique;

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }

    public boolean isLoginFailed(){
        return isLoginFailed;
    }

    public void setLoginFailed(boolean loginFailed){
        isLoginFailed = loginFailed;
    }

    public boolean isNameIsUnique(){
        return nameIsUnique;
    }

    public void setNameIsUnique(boolean nameIsUnique){
        this.nameIsUnique = nameIsUnique;
    }
}
