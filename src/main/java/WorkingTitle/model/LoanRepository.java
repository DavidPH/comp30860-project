package WorkingTitle.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;


import java.util.Date;
import java.util.Optional;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long>{
    List<Loan> findByUserId(int id);
    List<Loan> findByMediaId(int id);

    @Query("SELECT MAX(endDate) FROM Loan  WHERE MEDIA_ID = :mediaID")
    Date findLatestEndDateForMediaID(@Param("mediaID") int mediaID);

    @Query("SELECT loan from Loan loan WHERE MEDIA_ID = :mediaID AND USER_ID = :userID AND START_DATE < :currentDate AND END_DATE > :currentDate")
    Optional<Loan> findActiveLoanForMediaIdAndUserId(
            @Param("mediaID") int mediaID, @Param("userID") int userID, @Param("currentDate") Date currentDate);
}
