package WorkingTitle.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MediaRepository extends JpaRepository<Media, Long>{
    List<Media> findByTitleAndType(String title, String type);
    List<Media> findByGenreAndType(String genre, String type);
    List<Media> findByCreatorAndType(String creator, String type);
    Optional<Media> findById(int id);
}
