package WorkingTitle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkingTitleApplication{
    public static void main(String[] args){
        SpringApplication.run(WorkingTitleApplication.class, args);
    }
}
