package WorkingTitle.controller;

import WorkingTitle.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class WorkingTitleController{
    @Autowired
    private UserSession userSession;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MediaRepository mediaRepository;
    @Autowired
    private LoanRepository loanRepository;

    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("title", "Home");
        model.addAttribute("user", userSession.getUser());
        return "index.html";
    }

    @GetMapping("/catalogue")
    public String catalogue(Model model){
        model.addAttribute("title", "Catalogue");
        model.addAttribute("user", userSession.getUser());
        return "catalogue.html";
    }

    @GetMapping("/addMedia")
    public String addMediaPage(Model model, HttpServletResponse response) throws IOException{
        model.addAttribute("title", "Add Media");
        if(userSession.getUser() == null || (userSession.getUser() != null && userSession.getUser().getRole().equals("Member")))
            response.sendRedirect("/error401");
        model.addAttribute("user", userSession.getUser());
        return "addMedia.html";
    }

    @PostMapping("/addMedia")
    public void addMedia(Media newMedia, HttpServletResponse response) throws IOException{
        mediaRepository.save(newMedia);
        response.sendRedirect("/catalogue");
    }

    @RequestMapping(value="/editMedia", method= RequestMethod.POST)
    public String editMedia(@RequestParam("mediaId") int mediaId, Model model, HttpServletResponse response) throws IOException{
        if(userSession.getUser() == null || (userSession.getUser() != null && userSession.getUser().getRole().equals("Member")))
            response.sendRedirect("/error401");
        model.addAttribute("title", "Edit Media");
        model.addAttribute("user", userSession.getUser());
        Optional<Media> media = mediaRepository.findById(mediaId);

        if(media.isPresent()){
            model.addAttribute("media", media.get());
            return "editMedia.html";
        }else{
            return "redirect:/catalogue";
        }
    }

    @PostMapping("/changeStatus")
    public void changeStatus(int mediaId, String newStatus, HttpServletResponse response) throws IOException{
        Optional<Media> media = mediaRepository.findById(mediaId);
        if(media.isPresent()){
            if(newStatus.equals("Available")){
                List<Loan> mediaLoanList = loanRepository.findByMediaId(mediaId);
                Date currentDate = new Date(System.currentTimeMillis());
                long timeDifferential = 0;
                Loan currentLoan = null;
                // Had to find the current loan, so I could get the differential to apply to all the other loans
                for(Loan loan : mediaLoanList){
                    if(loan.getStartDate().before(currentDate) && loan.getEndDate().after(currentDate)){
                        timeDifferential = loan.getEndDate().getTime() - currentDate.getTime();
                        currentLoan = loan;
                        currentLoan.setEndDate(new Date(currentLoan.getEndDate().getTime() - timeDifferential));
                        loanRepository.save(currentLoan);
                        break;
                    }
                }

                boolean reservationFound = false;
                for(Loan loan: mediaLoanList){
                    if(loan.equals(currentLoan)){
                        continue;
                    }
                    if(loan.getStartDate().after(currentDate)){
                        if(!reservationFound)
                            reservationFound = true;
                        loan.setStartDate(new Date(loan.getStartDate().getTime() - timeDifferential));
                        loan.setEndDate(new Date(loan.getEndDate().getTime() - timeDifferential));
                        loanRepository.save(loan);
                    }
                }

                //Only needs to be changed if no reservation was found. On Loan status does not need changing otherwise.
                if(!reservationFound)
                    media.get().setStatus("Available");

            }else if(newStatus.equals("Discontinued")){
                List<Loan> mediaLoanList = loanRepository.findByMediaId(mediaId);
                Date currentDate = new Date(System.currentTimeMillis());
                for(Loan loan : mediaLoanList){
                    if(loan.getStartDate().before(currentDate) && loan.getEndDate().after(currentDate)){
                        long timeDifferential = loan.getEndDate().getTime() - currentDate.getTime();
                        loan.setEndDate(new Date(loan.getEndDate().getTime() - timeDifferential));
                    }else if(loan.getStartDate().after(currentDate)){
                        loanRepository.delete(loan);
                    }
                }
                media.get().setStatus(newStatus);
            }
            mediaRepository.save(media.get());
            response.sendRedirect("/catalogue");
        }
    }

    @PostMapping("/assignLoan")
    public void assignLoan(int mediaId, String username, HttpServletResponse response, RedirectAttributes attributes) throws IOException{
        List<User> user = userRepository.findByUsername(username);
        Optional<Media> media = mediaRepository.findById(mediaId);
        if(user.size() == 0){
            attributes.addAttribute(mediaId);
            response.sendRedirect("/");
        }else if(media.isPresent()){
            Media med = media.get();
            if(med.getStatus().equals("On Loan")){
                med.setStatus("Reserved");
            }else if(med.getStatus().equals("Available")){
                med.setStatus("On Loan");
            }
            Loan loan = new Loan();
            loan.setUser(user.get(0));
            loan.setMedia(med);
            long start;
            if(med.getStatus().equals("Reserved")){
                start = loanRepository.findLatestEndDateForMediaID(med.getId()).getTime();
            }else{
                start = System.currentTimeMillis();
            }
            loan.setStartDate(new Date(start));
            loan.setEndDate(new Date(start + (3*7*24*60*60*1000)));
            loanRepository.save(loan);
            mediaRepository.save(med);
            response.sendRedirect("/catalogue");
        }
    }

    @GetMapping("/searchMembers")
    public String searchMembers(Model model, HttpServletResponse response) throws IOException{
        if(userSession.getUser() == null || (userSession.getUser() != null && userSession.getUser().getRole().equals("Member")))
            response.sendRedirect("/error401");
        model.addAttribute("title", "Search Members");
        model.addAttribute("user", userSession.getUser());
        return "searchMembers.html";
    }

    @PostMapping("/searchMembers")
    public String doSearchMembers(String username,Model model){
        model.addAttribute("user", userSession.getUser());
        model.addAttribute("foundUsers", userRepository.findByUsername(username));
        return "searchMembers.html";
    }

    @GetMapping("/editMember")
    public String editMemberPage(@RequestParam(value="userID", required=false, defaultValue="-1") int userID, Model model){
        User user;
        User self = userSession.getUser();
        if (userID == -1)
            user = userSession.getUser();
        else
            user = userRepository.findById(userID).get();
        
        Boolean isSelf = self.getId() == user.getId();
        if (!isSelf){
            if (!self.getRole().equals("Librarian")){
                return "401.html";
            }
        }
        model.addAttribute("title", "Edit Member");
        model.addAttribute("isSelf", isSelf);
        model.addAttribute("member", user);
        model.addAttribute("user", self);
        if (userSession.isLoginFailed()){
            model.addAttribute("error", "Old Password is incorrect");
            userSession.setLoginFailed(false);
        }
        return "editMember.html";
    }

    @GetMapping("/profile")
    public String viewMemberPage(@RequestParam(value="userID", required=false, defaultValue="-1") int userID, Model model){
        User user = userSession.getUser();
        if (userID == -1){
            userID = user.getId();
        }
        User member = userRepository.findById(userID).get();
        Boolean isSelf = user.getId() == member.getId();
        if (isSelf || user.getRole().equals("Librarian")){
            model.addAttribute("member", member);
            model.addAttribute("user", user);
            List<Loan> allLoans = loanRepository.findByUserId(member.getId());
            List<Loan> pastLoans = new ArrayList<>();
            List<Loan> currentLoans = new ArrayList<>();
            List<Loan> reservations = new ArrayList<>();

            Date currentDate = new Date(System.currentTimeMillis());
            for (Loan loan : allLoans){
                if (currentDate.after(loan.getEndDate())){
                    pastLoans.add(loan);
                }
                else if (currentDate.before(loan.getStartDate())){
                    reservations.add(loan);
                }
                else{
                    currentLoans.add(loan);
                }
            }
            model.addAttribute("title", "Profile");
            model.addAttribute("pastLoans", pastLoans);
            model.addAttribute("currentLoans", currentLoans);
            model.addAttribute("reservations", reservations);
            return "viewMember.html";
        }
        return "401.html";
    }
    @PostMapping("/editMember")
    public void editProfile(@RequestParam(value="userID", required=true) int userID, String email, String username, String name, String oldpass, String newpass, String role, String DoB, HttpServletResponse response) throws IOException{
        User user = userRepository.findById(userID).get();
        User loggedInAs = userSession.getUser();
        if (loggedInAs == null)
            return; 
        if (loggedInAs.getId() != user.getId()){
            if (!loggedInAs.getRole().equals("Librarian")){
                return;
            }
        }
        
        
        if (newpass != null && oldpass != null){
            if (!newpass.equals("") && !oldpass.equals("")){
                if (oldpass.equals(user.getPassword())){
                    user.setPassword(newpass);
                }
                else{
                    userSession.setLoginFailed(true);
                    response.sendRedirect("/editMember");
                    return;
                }
            }
        }
        if (!name.equals(""))
            user.setName(name);
        if (!email.equals(""))
            user.setEmail(email);
        if (!username.equals(""))
            user.setUsername(username);
        if (!DoB.equals(""))
            user.setDateOfBirth(DoB);
        if (role != null && !role.equals(""))
            user.setRole(role);
        userRepository.save(user);
        response.sendRedirect("/profile?userID=" + user.getId());
    }

    @PostMapping("/searchCatalogue")
    public String searchCatalogue(String searchBy, String searchTerm, String type, Model model){
        List<Media> results = new ArrayList<>();
        model.addAttribute("user", userSession.getUser());
        String[] types = type.split(",");
        for(String t : types){
            switch(searchBy){
                case "title":
                    results.addAll(mediaRepository.findByTitleAndType(searchTerm, t));
                    break;
                case "genre":
                    results.addAll(mediaRepository.findByGenreAndType(searchTerm, t));
                    break;
                case "creator":
                    results.addAll(mediaRepository.findByCreatorAndType(searchTerm, t));
                    break;
                default:
                    System.out.println("Invalid search type " + searchBy + " - shouldn't be seeing this");
            }
        }
        model.addAttribute("title", "Search Results");
        model.addAttribute("media", results);
        model.addAttribute("allMedia", mediaRepository.findAll());
        return "search_results.html";
    }

    @RequestMapping(value="/borrow", method= RequestMethod.POST)
    public void createLoan(@RequestParam("id") int id, @RequestParam("userId") int userId, HttpServletResponse response) throws IOException{
        Optional<Media> media = mediaRepository.findById(id);
        Optional<User> user = userRepository.findById(userId);
        if(media.isPresent() && user.isPresent()){
            Media m = media.get();
            Optional<Loan> existingLoan = loanRepository.findActiveLoanForMediaIdAndUserId(m.getId(), userId, new Date(System.currentTimeMillis()));
            boolean renewable = (m.getStatus().equals("On Loan") && existingLoan.isPresent() );
            if(renewable){
                Loan e = existingLoan.get();
                e.setEndDate(new Date(e.getEndDate().getTime() + (7*24*60*60*1000))); // 1 week's worth of milliseconds
                loanRepository.save(e);   // implicitly updates existing loan entry
                response.sendRedirect("/profile");
            }else{
                if(m.getStatus().equals("On Loan")){
                    m.setStatus("Reserved");
                }else if(m.getStatus().equals("Available")){
                    m.setStatus("On Loan");
                }
                Loan loan = new Loan();
                loan.setMedia(m);
                loan.setUser(user.get());
                long start;
                if(m.getStatus().equals("Reserved")) {
                    start = loanRepository.findLatestEndDateForMediaID(m.getId()).getTime();
                }else {
                    start = System.currentTimeMillis();
                }
                loan.setStartDate(new Date(start));
                loan.setEndDate(new Date(start + (3*7*24*60*60*1000)));   // 3 week's worth of milliseconds
                loanRepository.save(loan);
                response.sendRedirect("/catalogue");
            }
        }else{
            response.sendRedirect("/");
        }
    }
}