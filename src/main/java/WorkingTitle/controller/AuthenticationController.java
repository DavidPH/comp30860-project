package WorkingTitle.controller;

import WorkingTitle.model.User;
import WorkingTitle.model.UserRepository;
import WorkingTitle.model.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
public class AuthenticationController{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserSession userSession;

    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("title", "Login");
        if(userSession.isLoginFailed()){
            model.addAttribute("error", "Username and password incorrect");
            userSession.setLoginFailed(false);
        }
        return "login.html";
    }

    @PostMapping("/login")
    public void doLogin(User inputUser, HttpServletResponse response) throws IOException{
        Optional<User> user = userRepository.findByUsernameAndPassword(inputUser.getUsername(),inputUser.getPassword());
        if(user.isPresent()){
            userSession.setUser(user.get());
            response.sendRedirect("/");
        }else{
            userSession.setLoginFailed(true);
            response.sendRedirect("/login");
        }
    }

    @GetMapping("/logout")
    public void logout(HttpServletResponse response) throws IOException{
        userSession.setUser(null);
        response.sendRedirect("/");
    }

    @GetMapping("/register")
    public String register(Model model){
        model.addAttribute("title", "Register");
        if(userSession.isNameIsUnique()){
            model.addAttribute("error", "Username is already taken");
            userSession.setNameIsUnique(false);
        }
        return "register.html";
    }

    @PostMapping("/register")
    public void doRegister(String email, String username, String name, String password, String role, String DoB, HttpServletResponse response) throws IOException{
        List<User> user = userRepository.findByUsername(username);
        if(user.size() == 0){
            userRepository.save(new User(username,name,email,password,role, DoB));
            response.sendRedirect("/login");
        }else{
            userSession.setNameIsUnique(true);
            response.sendRedirect("/register");
        }
    }

    @GetMapping("/error401")
    public String error401(Model model){
        model.addAttribute("title", "Error 401");
        return "error401.html";
    }
}
