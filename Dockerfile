FROM openjdk:8-jre-alpine

COPY target/project-0.0.1-SNAPSHOT.jar /project.jar
CMD java -jar project.jar
